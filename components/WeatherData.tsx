import {
  Box,
  Image,
  Flex,
  Text,
  Stack,
  Grid,
  GridItem,
  Wrap,
  WrapItem,
  Divider,
} from "@chakra-ui/react";
import Layout from "../components/layouts/layout";
import React from "react";
import { WeatherData } from "../https/fetch/getData";
import { NextPageWithLayout } from "../pages/page";
import DetailWeatherData from "./DetailWeatherData";
import {
  ResponsiveContainer,
  BarChart,
  Bar,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";
interface WeatherDataProps {
  WeatherDataList: WeatherData;
  DetailWeatherDataList: DetailWeatherData;
}

const WeatherDataPage: NextPageWithLayout<WeatherDataProps> = ({
  WeatherDataList,
  DetailWeatherDataList,
}) => {
  const sunriseDate = new Date(WeatherDataList?.sys?.sunrise * 1000);
  const sunriseTime = sunriseDate.toLocaleString("id-ID", {
    hour: "numeric",
    minute: "numeric",
    second: "numeric",
  });
  const sunsetDate = new Date(WeatherDataList?.sys?.sunset * 1000);
  const sunsetTime = sunsetDate.toLocaleString("id-ID", {
    hour: "numeric",
    minute: "numeric",
    second: "numeric",
  });
  const currentDate = new Date(WeatherDataList?.dt * 1000);
  const day = currentDate.getDate();
  const month = currentDate.getMonth() + 1;
  const year = currentDate.getFullYear();
  const hari = currentDate.toLocaleString("id-ID", { weekday: "long" });
  const iconUrl = `https://openweathermap.org/img/wn/${WeatherDataList?.weather[0]?.icon}.png`;

  const data = [
    { name: "Sunrise", InformasiTambahan: sunriseTime },
    { name: "Sunset", InformasiTambahan: sunsetTime },
    { name: "Max.Temp", InformasiTambahan: WeatherDataList.main.temp_min },
    { name: "Min.temp", InformasiTambahan: WeatherDataList.main.temp_max },
    { name: "Win.Deg", InformasiTambahan: WeatherDataList.wind.deg },
  ];

  return (
    <>
      {WeatherDataList && (
        <Box w="1400px">
          <Grid templateColumns="1.5fr 4.5fr">
            <GridItem>
              <Stack
                position="relative"
                bg="#1E1E1E"
                justifyContent="space-between"
                rounded="xl"
                p={8}
                w="430px"
                h="520px"
                backgroundImage="./assets/background.jpeg"
                backgroundSize="cover"
              >
                <Box
                  position="absolute"
                  top={0}
                  left={0}
                  w="100%"
                  h="100%"
                  bg="rgba(0, 0, 0, 0.3)"
                  rounded="xl"
                  zIndex={0}
                />
                <Stack zIndex={1}>
                  <Flex
                    justifyContent="space-between"
                    w="430px"
                    alignItems="center"
                  >
                    <Text fontSize="4xl" fontWeight="bold">
                      {hari}
                    </Text>
                  </Flex>
                  <Text>
                    {day}/{month}/{year}
                  </Text>
                  <Text> {WeatherDataList.name}</Text>
                </Stack>
                <Box zIndex={1}>
                  <Flex alignItems="end">
                    <Image
                      src={iconUrl}
                      alt="Weather Icon"
                      boxSize={50}
                      mr="2"
                    />
                    <Text mb="2">{WeatherDataList.main.temp}°C</Text>
                  </Flex>
                  <Flex
                    justifyContent="space-between"
                    alignItems="center"
                    fontSize="sm"
                  >
                    <Stack spacing="">
                      <Text> {WeatherDataList.weather[0].main}</Text>
                      <Text> {WeatherDataList.weather[0].description}</Text>
                    </Stack>
                    <Stack textAlign="right" spacing="0">
                      <Text>Lat: {WeatherDataList.coord.lat} </Text>
                      <Text>Long: {WeatherDataList.coord.lon}</Text>
                    </Stack>
                  </Flex>
                </Box>
              </Stack>
            </GridItem>
            <GridItem px={1} color="black">
              <Wrap justify="center" spacing="2">
                <WrapItem w="180px" bg="white" rounded="xl">
                  <Stack px={4} py={2} spacing={4}>
                    <Text color="#3F3F3F" fontWeight="bold">
                      Angin
                    </Text>
                    <Divider w="140px" borderBottomWidth="2px" />
                    <Text fontWeight="bold" fontSize="2xl">
                      {WeatherDataList.wind.speed} km/h
                    </Text>
                  </Stack>
                </WrapItem>
                <WrapItem w="180px" bg="white" rounded="xl">
                  <Stack px={4} py={2} spacing={4}>
                    <Text color="#3F3F3F" fontWeight="bold">
                      Kelembapan
                    </Text>
                    <Divider w="140px" borderBottomWidth="2px" />
                    <Text fontWeight="bold" fontSize="2xl">
                      {WeatherDataList.main.humidity}%
                    </Text>
                  </Stack>
                </WrapItem>
                <WrapItem w="180px" bg="white" rounded="xl">
                  <Stack px={4} py={2} spacing={4}>
                    <Text color="#3F3F3F" fontWeight="bold">
                      Jarak Pandang
                    </Text>
                    <Divider w="140px" borderBottomWidth="2px" />
                    <Text fontWeight="bold" fontSize="2xl">
                      {WeatherDataList.visibility} km
                    </Text>
                  </Stack>
                </WrapItem>
                <WrapItem w="180px" bg="white" rounded="xl">
                  <Stack px={4} py={2} spacing={4}>
                    <Text color="#3F3F3F" fontWeight="bold">
                      Tekanan
                    </Text>
                    <Divider w="140px" borderBottomWidth="2px" />
                    <Text fontWeight="bold" fontSize="2xl">
                      {WeatherDataList.main.pressure} mb
                    </Text>
                  </Stack>
                </WrapItem>
                <WrapItem w="180px" bg="white" rounded="xl">
                  <Stack px={4} py={2} spacing={4}>
                    <Text color="#3F3F3F" fontWeight="bold">
                      Titik Embun
                    </Text>
                    <Divider w="140px" borderBottomWidth="2px" />
                    <Text fontWeight="bold" fontSize="2xl">
                      {WeatherDataList.main.feels_like}°C
                    </Text>
                  </Stack>
                </WrapItem>
              </Wrap>
              <Box
                maxW="900px"
                mx="auto"
                mt={6}
                w="full"
                bg="white"
                px={12}
                py={8}
                rounded="xl"
              >
                <ResponsiveContainer width="100%" height={300}>
                  <BarChart data={data}>
                    <CartesianGrid strokeDasharray="3 3" />
                    <XAxis dataKey="name" />
                    <YAxis />
                    <Tooltip />
                    <Legend />
                    <Bar dataKey="InformasiTambahan" fill="#8884d8" />
                  </BarChart>
                </ResponsiveContainer>
              </Box>
            </GridItem>
          </Grid>
          <DetailWeatherData
            WeatherData={WeatherDataList}
            DetailWeatherDataList={DetailWeatherDataList}
            lat={WeatherDataList.coord.lat}
            lon={WeatherDataList.coord.lon}
          />
        </Box>
      )}
    </>
  );
};

WeatherDataPage.getLayout = (page) => {
  return <Layout pageTitle="Weather Apps">{page}</Layout>;
};

export default WeatherDataPage;
