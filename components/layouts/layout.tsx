import { Box, Container, Text } from "@chakra-ui/react";
import Head from "next/head";
import React, { ReactNode } from "react";

export default function Layout({
  children,
  pageTitle,
}: {
  children: ReactNode;
  pageTitle: string;
}) {
  return (
    <Box bg="#E5E5E5" minH="100vh">
      <Head>
        <title>{pageTitle}</title>
        <meta charSet="utf-8" />
        <meta name="viewport" content="initial-scale=1.0, width=device-width" />
      </Head>
      <Box bg="#2667FF" h="80px">
        <Container
          maxW="7xl"
          color="white"
          fontWeight="bold"
          h="full"
          display="flex"
          alignItems="center"
          bg="#2667FF"
        >
          <Text fontSize="2xl">Weather Apps</Text>
        </Container>
      </Box>
      <Container maxW="7xl">{children}</Container>
    </Box>
  );
}
