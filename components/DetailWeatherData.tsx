import {
  Box,
  Image,
  Flex,
  Text,
  Wrap,
  Divider,
  Stack,
  WrapItem,
  Button,
  useDisclosure,
  Modal,
  ModalBody,
  ModalCloseButton,
  ModalContent,
  ModalFooter,
  ModalHeader,
  ModalOverlay,
  Center,
  ListItem,
  UnorderedList,
} from "@chakra-ui/react";
import Layout from "../components/layouts/layout";
import React from "react";
import { useGetDetailData } from "../https/hooks/useGetDetailData";
import { WeatherData } from "../https/fetch/getData";
import { NextPageWithLayout } from "../pages/page";
import { DetailWeatherData } from "../https/fetch/getDetailData";
import { ArrowUpIcon, ArrowDownIcon } from "@chakra-ui/icons";

interface DetailWeatherDataProps {
  WeatherData: WeatherData;
  DetailWeatherDataList: DetailWeatherData;
  lat: number;
  lon: number;
}

const DetailWeatherData: NextPageWithLayout<DetailWeatherDataProps> = ({
  lat,
  lon,
  WeatherData,
}) => {
  const { data: DetailWeatherDataList } = useGetDetailData({
    lon: lon,
    lat: lat,
  });

  const { isOpen, onOpen, onClose } = useDisclosure();
  const [showAllData, setShowAllData] = React.useState(false);
  const iconUrl = `https://openweathermap.org/img/wn/${WeatherData?.weather[0]?.icon}.png`;
  const displayedData = showAllData
    ? DetailWeatherDataList?.list
    : DetailWeatherDataList?.list?.slice(0, 4);

  return (
    <Wrap my="12" justify="space-between" spacing="2">
      {displayedData?.map((index) => {
        const dt = new Date(index.dt * 1000);

        const days = [
          "Minggu",
          "Senin",
          "Selasa",
          "Rabu",
          "Kamis",
          "Jumat",
          "Sabtu",
        ];
        const day = days[dt.getDay()];
        const date = dt.getDate();
        const months = [
          "Januari",
          "Februari",
          "Maret",
          "April",
          "Mei",
          "Juni",
          "Juli",
          "Agustus",
          "September",
          "Oktober",
          "November",
          "Desember",
        ];
        const month = months[dt.getMonth()];
        const year = dt.getFullYear();
        const formattedDate = `${day}, ${date} ${month} ${year}`;

        return (
          <WrapItem w="340px" bg="white" rounded="xl" mb="12">
            <Stack px={4} py={2} spacing={4} w="full">
              <Text color="#3F3F3F" fontWeight="bold">
                {formattedDate}
              </Text>
              <Divider w="310px" borderBottomWidth="2px" />
              <Flex
                color="black"
                justifyContent="space-between"
                fontWeight="bold"
                fontSize="md"
                alignItems="center"
              >
                <Image src={iconUrl} alt="Weather Icon" boxSize={70} />
                <ArrowUpIcon w={8} h={8} mr="-5" />
                <Text>{index.main.temp_max}°C</Text>
                <ArrowDownIcon w={8} h={8} mr="-5" />
                <Text> {index.main.temp_min}°C</Text>
              </Flex>

              <Button
                fontWeight="bold"
                fontSize="sm"
                color="#2666FD"
                bg="#F5F7FA"
                onClick={onOpen}
              >
                More Info {">"}
              </Button>
              <Modal isOpen={isOpen} onClose={onClose} isCentered>
                <ModalOverlay />
                <ModalContent>
                  <ModalHeader>Detail Informasi</ModalHeader>
                  <ModalCloseButton />
                  <ModalBody>
                    <UnorderedList spacing={3}>
                      <ListItem>Temperature {index.main.temp}°C</ListItem>
                      <ListItem>
                        Temperature Maximal = {index.main.temp_max}°C
                      </ListItem>
                      <ListItem>
                        Temperature Minimal {index.main.temp_min}°C
                      </ListItem>
                      <ListItem>Besar Derajat {index.wind.deg}°</ListItem>
                      <ListItem>
                        Kecepatan Cuaca {index.wind.speed} m/s
                      </ListItem>
                      <ListItem>
                        Cuaca {index.weather.map((tex) => tex.main)}
                      </ListItem>
                      <ListItem>
                        Descripsi Cuaca{" "}
                        {index.weather.map((tex) => tex.description)}
                      </ListItem>
                    </UnorderedList>
                  </ModalBody>
                  <ModalFooter>
                    <Button colorScheme="red" mr={3} onClick={onClose} w="full">
                      Close
                    </Button>
                  </ModalFooter>
                </ModalContent>
              </Modal>
            </Stack>
          </WrapItem>
        );
      })}

      {!showAllData ? (
        <Center w="full">
          <Button
            mt={4}
            onClick={() => setShowAllData(true)}
            bg="#2666FD"
            colorScheme="blue"
          >
            Tampilkan Semuanya
          </Button>
        </Center>
      ) : (
        <Center w="full">
          <Button
            mt={4}
            onClick={() => setShowAllData(false)}
            bg="#2666FD"
            colorScheme="blue"
          >
            Tampilkan Lebih Sedikit
          </Button>
        </Center>
      )}
    </Wrap>
  );
};

DetailWeatherData.getLayout = (page) => {
  return <Layout pageTitle="Weather Apps">{page}</Layout>;
};

export default DetailWeatherData;
