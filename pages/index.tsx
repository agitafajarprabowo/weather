import { Box, Flex, Text, Select, Container } from "@chakra-ui/react";
import { NextPageWithLayout } from "./page";
import Layout from "../components/layouts/layout";
import React from "react";
import { useGetAllData } from "../https/hooks/useGetData";
import { useGetDetailData } from "../https/hooks/useGetDetailData";
import WeatherDataPage from "../components/WeatherData";

const cities = [
  { name: "Jakarta" },
  { name: "Bandung" },
  { name: "Semarang" },
  { name: "Surabaya" },
];

const IndexPage: NextPageWithLayout = () => {
  const [selectedCity, setSelectedCity] = React.useState(cities[""]);

  const { data: dataList } = useGetAllData({ city: selectedCity });
  const { data: detailDataList } = useGetDetailData({
    lon: dataList?.coord?.lon,
    lat: dataList?.coord?.lat,
  });

  return (
    <Box>
      <Flex align="center" justify="space-between" mb={4}>
        <Select
          width="full"
          value={selectedCity}
          onChange={(e) => setSelectedCity(e.target.value)}
          placeholder="Pilih Kota"
          variant="brandPrimary"
          mt="4"
        >
          {cities.map((city) => (
            <option key={city.name} value={city.name}>
              {city.name}
            </option>
          ))}
        </Select>
      </Flex>

      {dataList ? (
        <Container centerContent color="white">
          <WeatherDataPage
            WeatherDataList={dataList}
            DetailWeatherDataList={detailDataList}
          />
        </Container>
      ) : (
        <>
          <Text textAlign="center" color="black" fontSize="3xl">
            Silahkan pilih kota
          </Text>
        </>
      )}
    </Box>
  );
};

IndexPage.getLayout = (page) => {
  return <Layout pageTitle="Weather Apps">{page}</Layout>;
};

export default IndexPage;
