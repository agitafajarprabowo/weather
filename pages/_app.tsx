import "../styles/global.css";
import { ChakraBaseProvider, extendTheme } from "@chakra-ui/react";
import React from "react";
import { Hydrate, QueryClient, QueryClientProvider } from "react-query";

export const theme = extendTheme({
  config: {
    initialColorMode: "light",
    useSystemColorMode: false,
  },
});

const queryClient = new QueryClient();

export default function App({ Component, pageProps }) {
  const getLayout = Component.getLayout || ((page) => page);

  return (
    <QueryClientProvider client={queryClient}>
      <Hydrate state={pageProps.dehydratedState}>
        <ChakraBaseProvider theme={theme}>
          {getLayout(<Component {...pageProps} />)}
        </ChakraBaseProvider>
      </Hydrate>
    </QueryClientProvider>
  );
}
