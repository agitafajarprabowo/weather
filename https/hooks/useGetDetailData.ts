import { useQuery } from "react-query";
import { GetDetailDataParams, getDetailAllData } from "../fetch/getDetailData";

export const useGetDetailData = (params: GetDetailDataParams) => {
  return useQuery(["get-detail-data", params], () => getDetailAllData(params), {
    keepPreviousData: true,
    enabled: !!params.lat,
  });
};
