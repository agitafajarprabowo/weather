import { useQuery } from "react-query";
import { GetDataParams, getAllData } from "../fetch/getData";

export const useGetAllData = (params: GetDataParams) => {
  return useQuery(["get-data", params], () => getAllData(params), {
    keepPreviousData: true,
    enabled: !!params.city,
  });
};
