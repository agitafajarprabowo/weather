import axios from "axios";
import { BASE_URL, TOKEN } from "../../pages/api/api";

export interface DetailWeatherData {
  list: List[];
}

export interface List {
  dt: number;
  dt_text: string;
  main: {
    feels_like: number;
    grnd_level: number;
    humidity: number;
    pressure: number;
    sea_level: number;
    temp: number;
    temp_kf: number;
    temp_max: number;
    temp_min: number;
  };
  pop: number;
  rain: number;
  sys: string;
  visibility: number;
  weather: Weather[];
  wind: {
    deg: number;
    gust: number;
    speed: number;
  };
}

export interface Weather {
  id: number;
  main: string;
  description: string;
  icon: string;
}
export interface GetDetailDataParams {
  lon: number;
  lat: number;
}

export const getDetailAllData = async (
  params?: GetDetailDataParams
): Promise<DetailWeatherData> => {
  const response = await axios.get(`${BASE_URL}/forecast`, {
    params: {
      units: "metric",
      APPID: TOKEN,
      lat: params.lat,
      lon: params.lon,
    },
  });
  return response.data || [];
};
